package com.example.android.curso;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ActivityOne extends AppCompatActivity implements View.OnClickListener {

    private static final int RESULT_CODE = 1;
    private Button btnNewActivity;
    private Button btnDisplayInfo;
    private TextView txtResult;
    private String respuestaActUno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one);

        bindViews();
        setUpListeners();

        if (getIntent().getExtras() != null){
            respuestaActUno = getIntent().getExtras().getString("STRING_KEY");
        }

    }

    private void bindViews() {
        btnNewActivity = (Button) findViewById(R.id.one_btn_newActivity);
        btnDisplayInfo = (Button) findViewById(R.id.one_btn_display);
        txtResult = (TextView) findViewById(R.id.one_txt_result);
    }

    private void setUpListeners() {
        btnNewActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goActivityTwo(view);
            }                                          }
        );
        
        btnDisplayInfo.setOnClickListener(this);
    }
    
    public void goActivityTwo(View v) {
        Intent intent = new Intent(this, ActivityTwo.class);
        intent.putExtra("STRING_KEY", "VALOR");
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.one_btn_display:
                //Toast.makeText(ActivityOne.this, "Pendiente", Toast.LENGTH_SHORT).show();
                //Intent intentDisplay = new Intent(ActivityOne.this,ActivityTwo.class);
                //startActivityForResult(intentDisplay, RESULT_CODE);
                txtResult.setText(respuestaActUno);
                break;
        }
    }
}
