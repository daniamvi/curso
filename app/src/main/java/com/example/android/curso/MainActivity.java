package com.example.android.curso;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnMain = (Button) findViewById(R.id.main_btn_principal);
        btnMain.setOnClickListener(this);

        final String btnMainText = btnMain.getText().toString();

        /*btnMain.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, btnMainText, Toast.LENGTH_LONG).show();
                Log.e(TAG, "Ejecutamos onClick Listener del botón principal");
            }
        });*/
    }

    /*public void metodoUno(View v){
        Button btnMain = (Button) findViewById(R.id.main_btn_principal);
        Toast.makeText(this, "Has pulsado el botón", Toast.LENGTH_LONG).show();
        Log.e(TAG, "Ejecutamos metodoUno");
    }*/

    @Override
    protected void onStart() {
        super.onStart();
        Log.e(TAG, "Ejecutamos onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "Ejecutamos onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.e(TAG, "Ejecutamos onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e(TAG, "Ejecutamos onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "Ejecutamos onDestroy");
    }

    @Override
    public void onClick(View view) {
        Log.e(TAG, "Ejecutamos onClick a nivel de Clase");
        switch (view.getId()){
            case R.id.main_btn_principal:
                Toast.makeText(this,"dani",Toast.LENGTH_LONG);
                break;
            default:
                Toast.makeText(this,"default",Toast.LENGTH_LONG);
                break;
        }
    }
}
