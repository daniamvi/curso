package com.example.android.curso;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ActivityTwo extends AppCompatActivity {


    private Button butReturnAct1;
    private String value;
    private TextView txtEntrada;
    private EditText editResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two);
            
        bindViews();
        setUpListeners();

        if (getIntent().getExtras() != null){
            value = getIntent().getExtras().getString("STRING_KEY");
        }

        txtEntrada.setText(value);

    }

    private void bindViews() {
        butReturnAct1 = (Button) findViewById(R.id.two_btnVolver);
        txtEntrada = (TextView) findViewById(R.id.two_txt_entrada);
        editResultado = (EditText) findViewById(R.id.two_edit_respuesta);
    }

    private void setUpListeners() {
        butReturnAct1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goActivityOne(view);
            }                                          }
        );
    }

    private void goActivityOne(View view) {
        Intent intent = new Intent(this, ActivityOne.class);
        intent.putExtra("RESPUESTA", editResultado.getText().toString());
        startActivity(intent);
    }

}
